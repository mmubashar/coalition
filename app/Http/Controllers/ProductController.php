<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the products listing page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products_json = file_get_contents(base_path('storage/json/products.json'));
        $products = json_decode($products_json, true);
        $data = $this->getListing($products);
        return view('products.index')->with(compact('data'));
    }

    /**
     * Generic private method to prepare data for listing and also calculating sum of all totals.
     *
     * @return array
     */
    private function getListing($products){

        $products_collection = collect($products);
        $total =   $products_collection->sum(function ($product) {
            return $product['unit_price']*$product['quantity'];
        });
        $products = collect($products)->sortByDesc('created_at')->values()->all();
        return ['products' => $products, 'total' => $total];
    }

    /**
     * Create a new product.
     *
     * @return Bool
     * @throws
     */
    public function store(Request $request)
    {
        try {
            $products_json = file_get_contents(base_path('storage/json/products.json'));
            $products = json_decode($products_json, true);
            $sorted_products = array_values(Arr::sort($products, function ($value) {
                return $value['created_at'];
            }));

            $last_product = Arr::last($sorted_products);

            $product_data = Arr::except($request->all(), ['_token']);
            $product_data['created_at'] = Carbon::now()->toDateTimeString();
            $product_data['id'] = $last_product['id']+1;
            $products[] = $product_data;
//            dd($product_data);
            $products_json = json_encode($products, JSON_PRETTY_PRINT);
            file_put_contents(base_path('storage/json/products.json'), stripslashes($products_json));
            $data = $this->getListing($products);
            $html = view('products.partials.index.listing')->with(compact('data'))->render();
            return response()->json(array('success' => true, 'products_listing_html'=>$html));
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }

    /**
     * Edit product.
     *
     * @return Request $request
     */
    public function update(Request $request, $id)
    {
        try {
            $products_json = file_get_contents(base_path('storage/json/products.json'));
            $products = json_decode($products_json, true);
            $data = $request->all();
            foreach ($products as $key => $product) {
                if ($product['id'] == $id) {
                    $products[$key]['name'] = $data['name'];
                    $products[$key]['quantity'] = $data['quantity'];
                    $products[$key]['unit_price'] = $data['unit_price'];
                }
            }
            $products_json = json_encode($products);
            file_put_contents(base_path('storage/json/products.json'), stripslashes($products_json));
            $data = $this->getListing($products);
            $html = view('products.partials.index.listing')->with(compact('data'))->render();
            return response()->json(array('success' => true, 'products_listing_html'=>$html));
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }
}
