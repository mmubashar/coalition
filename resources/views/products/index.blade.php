<?php
/**
 * Created by PhpStorm.
 * User: mjamshad
 * Date: 6/17/2019
 * Time: 10:54 PM
 */
?>

@extends('layouts.app')

@section('css')
    <style>
        .products-table-row {
            margin-top: 50px;
        }

        .products-table-header {
            font-size: 3rem;
        }
    </style>

@endsection

@section('content')
    <div class="container">
        {{--Form--}}
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center">Add New Product</div>
                    <div class="container">
                        <form id="add_new_product">
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="name">Product Name</label>
                                    <input type="text" class="form-control" id="name"
                                           placeholder="Product Name" value="" required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="quantity">Quantity</label>
                                    <input type="number" class="form-control" id="quantity" placeholder="Quantity"
                                           value="0" required>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="unit_price">Unit Price</label>
                                    <input type="number" class="form-control" id="unit_price" value="0"
                                           placeholder="Unit Price" required>
                                </div>
                            </div>
                            <button class="btn btn-primary float-right" type="submit">Submit form</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        {{--Table--}}
        <div class="row justify-content-center products-table-row" id="products_listing">
            @include('products.partials.index.listing')
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $('#add_new_product').on('submit', function (event) {
                var data = {
                    name: $('#name').val(),
                    _token: "{{ csrf_token() }}",
                    quantity: $('#quantity').val(),
                    unit_price: $('#unit_price').val()
                };
                $.ajax({
                    type: 'POST',
                    url: "{{url('/products')}}",
                    data: data,
                    dataType: 'json'
                }).done(function(response){
                    if(response.success === true){
                        $('#products_listing').html(response.products_listing_html);
                    }
                    alert('Product created successfully!');
                });
                event.preventDefault();
            });
        });

        function editProduct(product_id) {
            var data = {
                name: $('#name_' + product_id).val(),
                _token: "{{ csrf_token() }}",
                _method: "put",
                quantity: $('#quantity_' + product_id).val(),
                unit_price: $('#unit_price_' + product_id).val()
            };
            $.ajax({
                type: 'POST',
                url: "{{url("/products")}}/" + product_id,
                data: data,
                dataType: 'json'
            }).done(function(response){
                if(response.success === true){
                    $('#products_listing').html(response.products_listing_html);
                }
                alert('Product updated successfully!');
            });
        }
    </script>
@endsection
