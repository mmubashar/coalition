<?php
/**
 * Created by PhpStorm.
 * User: mjamshad
 * Date: 6/18/2019
 * Time: 1:22 AM
 */
?>
<span class="products-table-header">Products Listing</span>
<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Quantity</th>
        <th scope="col">Unit Price</th>
        <th scope="col">Datetime Submitted</th>
        <th scope="col">Total Value</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($data['products'] as $key => $product)
        <tr id="{{$product['id']}}">
            <form id="edit_form_{{$product['id']}}">
                <th scope="row">{{++$key}}</th>
                <td><input type="text" class="form-control" id="name_{{$product['id']}}" placeholder="Name"
                           value="{{$product['name']}}" required></td>
                <td><input type="number" class="form-control" id="quantity_{{$product['id']}}" placeholder="Quantity"
                           value="{{$product['quantity']}}" required></td>
                <td><input type="number" class="form-control" id="unit_price_{{$product['id']}}"
                           placeholder="Unit Price"
                           value="{{$product['unit_price']}}" required></td>
                <td>{{$product['created_at']}}</td>
                <td>{{$product['unit_price'] * $product['quantity']}}</td>
                <td>
                    <button class="btn btn-info" onclick="editProduct({{$product['id']}})">Update</button>
                </td>
            </form>
        </tr>
    @endforeach
    <tr class="bg-secondary font-weight-bold"><td></td><td></td><td></td><td></td><td>Total</td><td>{{$data['total']}}</td><td></td>    </tr>
    </tbody>
</table>
